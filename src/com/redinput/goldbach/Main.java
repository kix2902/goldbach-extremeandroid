package com.redinput.goldbach;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main extends Activity implements OnClickListener {

	private Button btn;
	private EditText edit;
	private TextView txt;

	private ProgressDialog pd = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		btn = (Button) findViewById(R.id.btn);
		edit = (EditText) findViewById(R.id.edit);
		txt = (TextView) findViewById(R.id.txt);

		btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn:
			if ((edit.getText().toString().trim().length() == 0) || (Integer.parseInt(edit.getText().toString()) > Integer.MAX_VALUE) || (Integer.parseInt(edit.getText().toString()) <= 0)) {
				Toast.makeText(Main.this, "El numero debe estar entre 0 y " + Integer.MAX_VALUE, Toast.LENGTH_SHORT).show();

			} else {
				pd = ProgressDialog.show(Main.this, "Calculando", "Espere, por favor...", true, false);
				new Thread(runGoldbach).start();
			}

			break;

		}
	}

	Runnable runGoldbach = new Runnable() {

		@Override
		public void run() {
			final Long initMs = SystemClock.elapsedRealtime();

			final int N = Integer.parseInt(edit.getText().toString());

			boolean[] isprime = new boolean[N];

			for (int i = 2; i < N; i++)
				isprime[i] = true;

			// determinar numeros primos <N usando "Sieve of Eratosthenes"
			for (int i = 2; i * i < N; i++) {
				if (isprime[i]) {
					for (int j = i; i * j < N; j++)
						isprime[i * j] = false;
				}
			}

			// conteo de primos
			int primes = 0;
			for (int i = 2; i < N; i++)
				if (isprime[i])
					primes++;

			// almacenar primos en un listado
			int[] list = new int[primes];
			int n = 0;
			for (int i = 0; i < N; i++)
				if (isprime[i])
					list[n++] = i;

			// comprobar si 2 numeros primos sumados dan el numero final
			final ArrayList<String> listGoldbach = new ArrayList<String>();
			int left = 0, right = primes - 1;
			while (left < right) {
				if (list[left] + list[right] == N) {
					listGoldbach.add(list[left] + " + " + list[right]);
					left++;
					right--;
				} else if (list[left] + list[right] < N) {
					left++;
				} else {
					right--;
				}
			}

			final Long endMs = SystemClock.elapsedRealtime();

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					pd.dismiss();
					if (listGoldbach.size() > 0)
						txt.setText((endMs - initMs) + " milisegundos\n\nn1 menor: " + listGoldbach.get(0) + "\nn1 mayor: " + listGoldbach.get(listGoldbach.size() - 1));
					else
						txt.setText(N + " no es expresable como la suma de 2 primos");
				}
			});

		}
	};
}
